/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ee.ttu.idu0075.videoservice;

import com.sun.xml.rpc.processor.modeler.j2ee.xml.paramValueType;
import ee.ttu.idu0075._2015.ws.videoservice.*;
import java.math.BigInteger;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.POST;
import javax.ws.rs.PathParam;

/**
 * REST Web Service
 *
 * @author Genet
 */
@Path("videos")
public class VideosResource {

    @Context
    private UriInfo context;
    
    private Helpers helper = new Helpers();

    /**
     * Creates a new instance of VideoResource
     */
    public VideosResource() {
    }
    
    
    @GET
    @Path("/videos")
    @Produces("application/json")
    /**
     * example path
     * http://localhost:8080/VideoServiceWebApplication/webresources/videos/videos/?API_TOKEN=salajane
     */
    public GetVideoListResponse getVideoList(@QueryParam("API_TOKEN") String token) {
        VideoServiceWebApplication app = new VideoServiceWebApplication();
        GetVideoListRequest request = new GetVideoListRequest();
        request.setAPITOKEN(token);
        request.setToken(helper.getRandomToken());
        
        return app.getVideoList(request);
    }
    
    @GET
    @Path("/channels")
    @Produces("application/json")
    /**
     * example path 
     * http://localhost:8080/VideoServiceWebApplication/webresources/videos/channels/?API_TOKEN=salajane&isPartner=yes&joined=2012-02-03
     */
    public GetVideoChannelListResponse getVideoChannelList(@QueryParam("API_TOKEN") String token,
            @QueryParam("isPartner") String isPartner,
            @QueryParam("joined") String joined
            ) {
        VideoServiceWebApplication app = new VideoServiceWebApplication();
        GetVideoChannelListRequest request = new GetVideoChannelListRequest();
        request.setAPITOKEN(token);
        request.setIsPartner(helper.isPartner(isPartner));
        request.setJoined(helper.stringToXMLDate(joined));
        request.setToken(helper.getRandomToken());
        return app.getVideoChannelList(request);
    }
    
    @GET
    @Path("{id : \\d+}") //support digit only
    @Produces("application/json")
    /**
     * example path
     * http://localhost:8080/VideoServiceWebApplication/webresources/videos/1/?API_TOKEN=salajane
     */
    public VideoType getVideo(@PathParam("id") String id,
            @QueryParam("API_TOKEN") String token) {
        VideoServiceWebApplication app = new VideoServiceWebApplication();
        GetVideoRequest request = new GetVideoRequest();
        request.setAPITOKEN(token);
        request.setId(BigInteger.valueOf(Integer.parseInt(id)));
        request.setToken(helper.getRandomToken());
        return app.getVideo(request);
    }
    
    @GET
    @Path("/channel/{id : \\d+}") //support digit only
    @Produces("application/json")
    /**
     * example path
     * http://localhost:8080/VideoServiceWebApplication/webresources/videos/channel/1/?API_TOKEN=salajane
     */
    public ChannelType getVideoChannel(@PathParam("id") String id,
            @QueryParam("API_TOKEN") String token) {
        VideoServiceWebApplication app = new VideoServiceWebApplication();
        GetVideoChannelRequest request = new GetVideoChannelRequest();
        request.setAPITOKEN(token);
        request.setId(BigInteger.valueOf(Integer.parseInt(id)));
        request.setToken(helper.getRandomToken());
        return app.getVideoChannel(request);
    }
    
    @GET
    @Path("/channel/videos/{id : \\d+}") //support digit only
    @Produces("application/json")
    /**
     * example path
     * http://localhost:8080/VideoServiceWebApplication/webresources/videos/channel/videos/1/?API_TOKEN=salajane
     */
    public ChannelVideoListType getChannelVideoList(@PathParam("id") String id,
            @QueryParam("API_TOKEN") String token) {
        VideoServiceWebApplication app = new VideoServiceWebApplication();
        GetChannelVideoListRequest request = new GetChannelVideoListRequest();
        request.setAPITOKEN(token);
        request.setChannelId(BigInteger.valueOf(Integer.parseInt(id)));
        request.setToken(helper.getRandomToken());
        return app.getChannelVideoList(request);
    }
    
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    /**
     * Example path
     * http://localhost:8080/VideoServiceWebApplication/webresources/videos/?API_TOKEN=salajane
     * {
	"id": 0,
	"name": "SOAP Tutorial",
	"length": 30,
	"uploadedAt": "2012-02-03"
        }
     */
    public VideoType addVideo(VideoType data, @QueryParam("API_TOKEN") String token) {
        VideoServiceWebApplication app = new VideoServiceWebApplication();
        AddVideoRequest request = new AddVideoRequest();
        request.setAPITOKEN(token);
        request.setName(data.getName());
        request.setLength(data.getLength());
        request.setUploadedAt(data.getUploadedAt());
        request.setToken(helper.getRandomToken());
        
        return app.addVideo(request);
    }
    
    @POST
    @Path("/channels")
    @Consumes("application/json")
    @Produces("application/json")
    /**
     * Example path
     * http://localhost:8080/VideoServiceWebApplication/webresources/videos/channels/?API_TOKEN=salajane
     {
	"id": 0,
	"channelName": "Pewds",
	"isPartner": "YES",
	"numberOfVideos": 1,
	"joined": "2016-02-16"
     }
     */
    public ChannelType addVideoChannel(ChannelType data, @QueryParam("API_TOKEN") String token) {
        VideoServiceWebApplication app = new VideoServiceWebApplication();
        AddVideoChannelRequest request = new AddVideoChannelRequest();
        request.setAPITOKEN(token);
        request.setChannelName(data.getChannelName());
        request.setIsPartner(data.getIsPartner());
        request.setNumberOfVideos(data.getNumberOfVideos());
        request.setJoined(data.getJoined());
        request.setToken(helper.getRandomToken());
        return app.addVideoChannel(request);  
    }
    
    @POST
    @Path("/channelVideo")
    @Consumes("application/json")
    @Produces("application/json")
    /**
     * Example path
     * http://localhost:8080/VideoServiceWebApplication/webresources/videos/channelVideo/?API_TOKEN=salajane
     * {
	"channelId": 1,
	"videoId": 1
       }
     */
    public ChannelVideoType addChannelVideo(AddChannelVideoRequest data,
            @QueryParam("API_TOKEN") String token) {
        VideoServiceWebApplication app = new VideoServiceWebApplication();
        AddChannelVideoRequest request = new AddChannelVideoRequest();
        request.setAPITOKEN(token);
        request.setChannelId(data.getChannelId());
        request.setVideoId(data.getVideoId());
        request.setToken(helper.getRandomToken());
        return app.addChannelVideo(request);     
    }
}
