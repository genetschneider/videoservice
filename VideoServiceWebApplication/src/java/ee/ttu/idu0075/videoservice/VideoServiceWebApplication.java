/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ee.ttu.idu0075.videoservice;

import ee.ttu.idu0075._2015.ws.videoservice.*;
import ee.ttu.idu0075._2015.ws.videoservice.VideoType;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.jws.WebService;

/**
 *
 * @author Genet Schneider 142678
 */
@WebService(serviceName = "VideoService", portName = "VideoPort", endpointInterface = "ee.ttu.idu0075._2015.ws.videoservice.VideoPortType", targetNamespace = "http://www.ttu.ee/idu0075/2015/ws/videoService", wsdlLocation = "WEB-INF/wsdl/VideoServiceWebApplication/videoService.wsdl")
public class VideoServiceWebApplication {

    private static int videoId = 1;
    private static int channelId = 1;
    private static List<VideoType> videos = new ArrayList<>();
    private static List<ChannelType> channels = new ArrayList<>();
    private static List<String> tokens = new ArrayList<String>();
    private Helpers helper = new Helpers();
    
    /**
     * gets video
     * @param parameter
     * @return 
     */
    public VideoType getVideo(GetVideoRequest parameter) {
        VideoType videoType = null;
        if (parameter.getAPITOKEN().equalsIgnoreCase("salajane") && !tokens.contains(parameter.getToken())) {
            for (int i = 0; i < videos.size(); i++) {
                if (videos.get(i).getId().equals(parameter.getId())) {
                    videoType = videos.get(i);
                    tokens.add(parameter.getToken());
                    return videoType;
                }
            }
        }
        
        return videoType;
    }
    
    /**
     * Adds video
     * @param parameter
     * @return 
     */
    public VideoType addVideo(AddVideoRequest parameter) {
        VideoType videoType = new VideoType();
        if (parameter.getAPITOKEN().equalsIgnoreCase("salajane") && !tokens.contains(parameter.getToken())) {
            if (parameter.getName() != null 
                    && parameter.getLength() != null && parameter.getUploadedAt() != null) {
                
                videoType = new VideoType();
                videoType.setId(BigInteger.valueOf(videoId++));
                videoType.setName(parameter.getName());
                videoType.setLength(parameter.getLength());
                videoType.setUploadedAt(parameter.getUploadedAt());
                tokens.add(parameter.getToken());
                videos.add(videoType);
            }

        }
        
        return videoType;
    }
    
    /**
     * gets video list
     * @param parameter
     * @return 
     */
    public GetVideoListResponse getVideoList(GetVideoListRequest parameter) {
        GetVideoListResponse response = new GetVideoListResponse();
        if (parameter.getAPITOKEN().equalsIgnoreCase("salajane") && !tokens.contains(parameter.getToken())) {
            tokens.add(parameter.getToken());
            for (VideoType video: videos) {
                response.getVideo().add(video);
            }
        }
        
        return response;
    }
    
    /**
     * gets video channel by filter
     * @param parameter
     * @return 
     */
    public ChannelType getVideoChannel(GetVideoChannelRequest parameter) {
        if (parameter.getAPITOKEN().equalsIgnoreCase("salajane") && !tokens.contains(parameter.getToken())) {
            for (int i = 0; i < channels.size(); i++) {
                if (channels.get(i).getId().equals(parameter.getId())) {
                    tokens.add(parameter.getToken());
                    return channels.get(i);
                }
            }
        }
        
        return null;
    }
    
    /**
     * adds video channel
     * @param parameter
     * @return 
     */
    public ChannelType addVideoChannel(AddVideoChannelRequest parameter) {
        ChannelType channel = new ChannelType();
        if (parameter.getAPITOKEN().equalsIgnoreCase("salajane") && !tokens.contains(parameter.getToken())) {
            if (parameter.getChannelName() != null && parameter.getIsPartner() != null 
                    && parameter.getJoined() != null) {
                channel.setId(BigInteger.valueOf(channelId++));
                channel.setChannelName(parameter.getChannelName());
                channel.setIsPartner(parameter.getIsPartner());
                channel.setJoined(parameter.getJoined());
                channel.setNumberOfVideos(parameter.getNumberOfVideos());
                channel.setChannelVideoList(new ChannelVideoListType());
                tokens.add(parameter.getToken());
                channels.add(channel);
            }
        }
        
        return channel;
    }
    
    /**
     * gets list of video channels
     * @param parameter
     * @return 
     */
    public GetVideoChannelListResponse getVideoChannelList(GetVideoChannelListRequest parameter) {
        GetVideoChannelListResponse response = new GetVideoChannelListResponse();
        if (parameter.getAPITOKEN().equalsIgnoreCase("salajane") && !tokens.contains(parameter.getToken())) {
            tokens.add(parameter.getToken());
            if (parameter.getIsPartner() != null && parameter.getJoined() != null) {
                for (ChannelType channel: channels) {
                    /* process partner channel */
                    if (!response.getVideoChannel().contains(channel)) {
                        if (channel.getIsPartner() == parameter.getIsPartner() 
                                && channel.getJoined().getYear() == parameter.getJoined().getYear()) {
                            response.getVideoChannel().add(channel);
                        }
                    }
                    
                }
            }
        }
        
        return response;
    }
    
    /**
     * gets list of channels videos
     * @param parameter
     * @return 
     */
    public ChannelVideoListType getChannelVideoList(GetChannelVideoListRequest parameter) {
        ChannelVideoListType channelVideoList = new ChannelVideoListType();
        if (parameter.getAPITOKEN().equalsIgnoreCase("salajane") && !tokens.contains(parameter.getToken())) {
            tokens.add(parameter.getToken());
            for (int i = 0; i < channels.size(); i++) {
                if (channels.get(i).getId().equals(parameter.getChannelId())) {
                    channelVideoList = channels.get(i).getChannelVideoList();
                }
            }
        }
        
        return channelVideoList; 
    }
    
    /**
     * add video to channel
     * @param parameter
     * @return 
     */
    public ChannelVideoType addChannelVideo(AddChannelVideoRequest parameter) {
        ChannelVideoType channelVideo = new ChannelVideoType();
        VideoType video = null;
        ChannelType channel = null;
        
        if (parameter.getAPITOKEN().equalsIgnoreCase("salajane") && !tokens.contains(parameter.getToken())) {
            tokens.add(parameter.getToken());
            /* find video */
            for (int i = 0; i < videos.size(); i++) {
                if (videos.get(i).getId().equals(parameter.getVideoId())) {
                    video = videos.get(i);
                }
            }
            
            /* find channel */
            for (int i = 0; i < channels.size(); i++) {
                if (channels.get(i).getId().equals(parameter.getChannelId())) {
                    channel = channels.get(i);
                }
            }
            
            if (video != null && channel != null) {
                channelVideo.setChannelVideo(video);
                channel.getChannelVideoList().getChannelVideo().add(channelVideo);
                channel.setNumberOfVideos(channel.getNumberOfVideos().add(BigInteger.ONE));
            }
        }
        
        return channelVideo;
    }
}
