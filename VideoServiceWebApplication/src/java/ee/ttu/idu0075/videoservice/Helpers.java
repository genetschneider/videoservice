/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ee.ttu.idu0075.videoservice;

    
import java.security.SecureRandom;
import java.math.BigInteger;
import ee.ttu.idu0075._2015.ws.videoservice.IsPartnerType;
import ee.ttu.idu0075.videoservice.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 *
 * @author Genet
 */
public class Helpers {
    
    /**
     * get random number in range of max and min
     * @param min
     * @param max
     * @return 
     */
    public int getRandomNumberInRange(int min, int max) {
        Random rand = new Random();
        int randomNumber = rand.nextInt((max - min) + 1) + min;

        return randomNumber;
    }
    
    public XMLGregorianCalendar getXMLDate() {
        try {
            return DatatypeFactory.newInstance().newXMLGregorianCalendar(new GregorianCalendar());
        } catch (DatatypeConfigurationException ex) {
            ex.printStackTrace();
        }
        
        return null;
    }
    
    public IsPartnerType isPartner(String str) {
        if (str.equalsIgnoreCase("yes")) {
            return IsPartnerType.YES;
        }
        
        return IsPartnerType.NO;
    }
    
    public XMLGregorianCalendar stringToXMLDate(String dateStr) {
        try { 
            DateFormat formatter;
            formatter = new SimpleDateFormat("yy-MM-dd");
            Date date = formatter.parse(dateStr);
            GregorianCalendar gregory = new GregorianCalendar();
            gregory.setTime(date);
            return DatatypeFactory.newInstance().newXMLGregorianCalendar(gregory);
        } catch (ParseException ex) {
            Logger.getLogger(Helpers.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DatatypeConfigurationException ex) {
            Logger.getLogger(Helpers.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

  
    public String getRandomToken() {
        SecureRandom random = new SecureRandom();
        return new BigInteger(130, random).toString(32);
    }

}
