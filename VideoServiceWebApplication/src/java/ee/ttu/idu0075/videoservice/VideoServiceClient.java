/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ee.ttu.idu0075.videoservice;
import ee.ttu.idu0075._2015.ws.videoservice.*;
import java.math.BigInteger;
import java.util.List;

/**
 *
 * @author Genet Schneider
 */
public class VideoServiceClient {
    /**
     * Helper
     */
    private Helpers helper = new Helpers();
    
    /**
     * Application
     */
    private VideoServiceWebApplication app = new VideoServiceWebApplication();
    
    public static void main(String[] args) {
        VideoServiceClient client = new VideoServiceClient();
        
        /* add video */
        client.createAddVideoRequest();
        
        /* add channel */
        client.createAddChannelRequest();
        
        /* link channel and video */
        client.linkChannelAndVideo();
        
        /* get info */
        client.printVideoInfo();
        client.printChannelInfo(); 
    }
    
    /**
     * Create add video request
     */
    public void createAddVideoRequest() {
        AddVideoRequest request = new AddVideoRequest();
        request.setAPITOKEN("salajane");
        request.setName("PHP Tutorial");
        request.setLength(BigInteger.valueOf(10));
        request.setUploadedAt(helper.getXMLDate());
        request.setToken(helper.getRandomToken());
        addVideo(request);
    }
    
    /**
     * adds video via application
     * @param request
     * @return 
     */
    public VideoType addVideo(AddVideoRequest request) {
       
        return app.addVideo(request);
    }
    
    /**
     * creates add channel request
     */
    public void createAddChannelRequest() {
        AddVideoChannelRequest request = new AddVideoChannelRequest();
        request.setAPITOKEN("salajane");
        request.setChannelName("PhpTeacher123");
        request.setIsPartner(IsPartnerType.NO);
        request.setJoined(helper.getXMLDate());
        request.setNumberOfVideos(BigInteger.ZERO);
        request.setToken(helper.getRandomToken());
        addChannel(request);
    }
    
    /**
     * adds channel via application
     * @param request
     * @return 
     */
    public ChannelType addChannel(AddVideoChannelRequest request) {
        
        return app.addVideoChannel(request);
    }
    
    /**
     * Links channel and video together
     * @return 
     */
    public ChannelVideoType linkChannelAndVideo() {
        AddChannelVideoRequest request = new AddChannelVideoRequest();
        request.setAPITOKEN("salajane");
        request.setChannelId(BigInteger.ONE);
        request.setVideoId(BigInteger.ONE);
        request.setToken(helper.getRandomToken());
        
        return app.addChannelVideo(request);
    }
    
    /**
     * Prints information about videos
     */
    public void printVideoInfo() {
        GetVideoListRequest request = new GetVideoListRequest();
        request.setAPITOKEN("salajane");
        request.setToken(helper.getRandomToken());
        
        List<VideoType> videos = app.getVideoList(request).getVideo();
        
        for (VideoType video: videos) {
            System.out.println("Video id: " + video.getId() 
                    + ", name: " + video.getName() 
                    + ", length: " + video.getLength()
                    + ", upladed at: " + video.getUploadedAt());
        }
        
    }
    
    /**
     * Prints information about channels
     */
    public void printChannelInfo() {
        GetVideoChannelListRequest request = new GetVideoChannelListRequest();
        request.setAPITOKEN("salajane");
        request.setIsPartner(IsPartnerType.NO);
        request.setJoined(helper.getXMLDate());
        request.setToken(helper.getRandomToken());
        
        List<ChannelType> channels = app.getVideoChannelList(request).getVideoChannel();
        
        for (ChannelType channel: channels) {
            System.out.println("Channel id: " + channel.getId()
            + ", channel name: " + channel.getChannelName()
            + ", is partner: " + channel.getIsPartner()
            + ", no. of videos: " + channel.getNumberOfVideos()
            + ", joined at: " + channel.getJoined()
            + ", videos ");
            for (int i = 0; i < channel.getChannelVideoList().getChannelVideo().size(); i++) {
                VideoType video = channel.getChannelVideoList().getChannelVideo().get(i).getChannelVideo();
                System.out.println("\tVideo id: " + video.getId() 
                    + ", name: " + video.getName() 
                    + ", length: " + video.getLength()
                    + ", upladed at: " + video.getUploadedAt());
            }
        }
    }
}
