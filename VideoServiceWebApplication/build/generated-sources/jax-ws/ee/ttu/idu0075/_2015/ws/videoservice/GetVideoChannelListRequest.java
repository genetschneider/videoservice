
package ee.ttu.idu0075._2015.ws.videoservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="API_TOKEN" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="token" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="isPartner" type="{http://www.ttu.ee/idu0075/2015/ws/videoService}IsPartnerType"/&gt;
 *         &lt;element name="joined" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *         &lt;element name="hasRelatedVideos" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;pattern value="YES|NO"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "apitoken",
    "token",
    "isPartner",
    "joined",
    "hasRelatedVideos"
})
@XmlRootElement(name = "getVideoChannelListRequest")
public class GetVideoChannelListRequest {

    @XmlElement(name = "API_TOKEN", required = true)
    protected String apitoken;
    @XmlElement(required = true)
    protected String token;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected IsPartnerType isPartner;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar joined;
    protected String hasRelatedVideos;

    /**
     * Gets the value of the apitoken property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAPITOKEN() {
        return apitoken;
    }

    /**
     * Sets the value of the apitoken property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAPITOKEN(String value) {
        this.apitoken = value;
    }

    /**
     * Gets the value of the token property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToken() {
        return token;
    }

    /**
     * Sets the value of the token property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToken(String value) {
        this.token = value;
    }

    /**
     * Gets the value of the isPartner property.
     * 
     * @return
     *     possible object is
     *     {@link IsPartnerType }
     *     
     */
    public IsPartnerType getIsPartner() {
        return isPartner;
    }

    /**
     * Sets the value of the isPartner property.
     * 
     * @param value
     *     allowed object is
     *     {@link IsPartnerType }
     *     
     */
    public void setIsPartner(IsPartnerType value) {
        this.isPartner = value;
    }

    /**
     * Gets the value of the joined property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getJoined() {
        return joined;
    }

    /**
     * Sets the value of the joined property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setJoined(XMLGregorianCalendar value) {
        this.joined = value;
    }

    /**
     * Gets the value of the hasRelatedVideos property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHasRelatedVideos() {
        return hasRelatedVideos;
    }

    /**
     * Sets the value of the hasRelatedVideos property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHasRelatedVideos(String value) {
        this.hasRelatedVideos = value;
    }

}
