
package ee.ttu.idu0075._2015.ws.videoservice;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for channelType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="channelType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="channelName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="isPartner" type="{http://www.ttu.ee/idu0075/2015/ws/videoService}IsPartnerType"/&gt;
 *         &lt;element name="numberOfVideos" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="channelVideoList" type="{http://www.ttu.ee/idu0075/2015/ws/videoService}channelVideoListType"/&gt;
 *         &lt;element name="joined" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "channelType", propOrder = {
    "id",
    "channelName",
    "isPartner",
    "numberOfVideos",
    "channelVideoList",
    "joined"
})
public class ChannelType {

    @XmlElement(required = true)
    protected BigInteger id;
    @XmlElement(required = true)
    protected String channelName;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected IsPartnerType isPartner;
    @XmlElement(required = true)
    protected BigInteger numberOfVideos;
    @XmlElement(required = true)
    protected ChannelVideoListType channelVideoList;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar joined;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setId(BigInteger value) {
        this.id = value;
    }

    /**
     * Gets the value of the channelName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChannelName() {
        return channelName;
    }

    /**
     * Sets the value of the channelName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChannelName(String value) {
        this.channelName = value;
    }

    /**
     * Gets the value of the isPartner property.
     * 
     * @return
     *     possible object is
     *     {@link IsPartnerType }
     *     
     */
    public IsPartnerType getIsPartner() {
        return isPartner;
    }

    /**
     * Sets the value of the isPartner property.
     * 
     * @param value
     *     allowed object is
     *     {@link IsPartnerType }
     *     
     */
    public void setIsPartner(IsPartnerType value) {
        this.isPartner = value;
    }

    /**
     * Gets the value of the numberOfVideos property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumberOfVideos() {
        return numberOfVideos;
    }

    /**
     * Sets the value of the numberOfVideos property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumberOfVideos(BigInteger value) {
        this.numberOfVideos = value;
    }

    /**
     * Gets the value of the channelVideoList property.
     * 
     * @return
     *     possible object is
     *     {@link ChannelVideoListType }
     *     
     */
    public ChannelVideoListType getChannelVideoList() {
        return channelVideoList;
    }

    /**
     * Sets the value of the channelVideoList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChannelVideoListType }
     *     
     */
    public void setChannelVideoList(ChannelVideoListType value) {
        this.channelVideoList = value;
    }

    /**
     * Gets the value of the joined property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getJoined() {
        return joined;
    }

    /**
     * Sets the value of the joined property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setJoined(XMLGregorianCalendar value) {
        this.joined = value;
    }

}
