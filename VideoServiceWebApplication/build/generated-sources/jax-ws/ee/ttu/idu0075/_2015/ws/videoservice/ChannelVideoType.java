
package ee.ttu.idu0075._2015.ws.videoservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for channelVideoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="channelVideoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="channelVideo" type="{http://www.ttu.ee/idu0075/2015/ws/videoService}videoType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "channelVideoType", propOrder = {
    "channelVideo"
})
public class ChannelVideoType {

    @XmlElement(required = true)
    protected VideoType channelVideo;

    /**
     * Gets the value of the channelVideo property.
     * 
     * @return
     *     possible object is
     *     {@link VideoType }
     *     
     */
    public VideoType getChannelVideo() {
        return channelVideo;
    }

    /**
     * Sets the value of the channelVideo property.
     * 
     * @param value
     *     allowed object is
     *     {@link VideoType }
     *     
     */
    public void setChannelVideo(VideoType value) {
        this.channelVideo = value;
    }

}
