
package ee.ttu.idu0075._2015.ws.videoservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ee.ttu.idu0075._2015.ws.videoservice package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetVideoResponse_QNAME = new QName("http://www.ttu.ee/idu0075/2015/ws/videoService", "getVideoResponse");
    private final static QName _AddVideoResponse_QNAME = new QName("http://www.ttu.ee/idu0075/2015/ws/videoService", "addVideoResponse");
    private final static QName _GetVideoChannelResponse_QNAME = new QName("http://www.ttu.ee/idu0075/2015/ws/videoService", "getVideoChannelResponse");
    private final static QName _AddVideoChannelResponse_QNAME = new QName("http://www.ttu.ee/idu0075/2015/ws/videoService", "addVideoChannelResponse");
    private final static QName _GetChannelVideoListResponse_QNAME = new QName("http://www.ttu.ee/idu0075/2015/ws/videoService", "getChannelVideoListResponse");
    private final static QName _AddChannelVideoResponse_QNAME = new QName("http://www.ttu.ee/idu0075/2015/ws/videoService", "addChannelVideoResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ee.ttu.idu0075._2015.ws.videoservice
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetVideoRequest }
     * 
     */
    public GetVideoRequest createGetVideoRequest() {
        return new GetVideoRequest();
    }

    /**
     * Create an instance of {@link VideoType }
     * 
     */
    public VideoType createVideoType() {
        return new VideoType();
    }

    /**
     * Create an instance of {@link AddVideoRequest }
     * 
     */
    public AddVideoRequest createAddVideoRequest() {
        return new AddVideoRequest();
    }

    /**
     * Create an instance of {@link GetVideoListRequest }
     * 
     */
    public GetVideoListRequest createGetVideoListRequest() {
        return new GetVideoListRequest();
    }

    /**
     * Create an instance of {@link GetVideoListResponse }
     * 
     */
    public GetVideoListResponse createGetVideoListResponse() {
        return new GetVideoListResponse();
    }

    /**
     * Create an instance of {@link GetVideoChannelRequest }
     * 
     */
    public GetVideoChannelRequest createGetVideoChannelRequest() {
        return new GetVideoChannelRequest();
    }

    /**
     * Create an instance of {@link ChannelType }
     * 
     */
    public ChannelType createChannelType() {
        return new ChannelType();
    }

    /**
     * Create an instance of {@link AddVideoChannelRequest }
     * 
     */
    public AddVideoChannelRequest createAddVideoChannelRequest() {
        return new AddVideoChannelRequest();
    }

    /**
     * Create an instance of {@link GetVideoChannelListRequest }
     * 
     */
    public GetVideoChannelListRequest createGetVideoChannelListRequest() {
        return new GetVideoChannelListRequest();
    }

    /**
     * Create an instance of {@link GetVideoChannelListResponse }
     * 
     */
    public GetVideoChannelListResponse createGetVideoChannelListResponse() {
        return new GetVideoChannelListResponse();
    }

    /**
     * Create an instance of {@link GetChannelVideoListRequest }
     * 
     */
    public GetChannelVideoListRequest createGetChannelVideoListRequest() {
        return new GetChannelVideoListRequest();
    }

    /**
     * Create an instance of {@link ChannelVideoListType }
     * 
     */
    public ChannelVideoListType createChannelVideoListType() {
        return new ChannelVideoListType();
    }

    /**
     * Create an instance of {@link AddChannelVideoRequest }
     * 
     */
    public AddChannelVideoRequest createAddChannelVideoRequest() {
        return new AddChannelVideoRequest();
    }

    /**
     * Create an instance of {@link ChannelVideoType }
     * 
     */
    public ChannelVideoType createChannelVideoType() {
        return new ChannelVideoType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VideoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ttu.ee/idu0075/2015/ws/videoService", name = "getVideoResponse")
    public JAXBElement<VideoType> createGetVideoResponse(VideoType value) {
        return new JAXBElement<VideoType>(_GetVideoResponse_QNAME, VideoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VideoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ttu.ee/idu0075/2015/ws/videoService", name = "addVideoResponse")
    public JAXBElement<VideoType> createAddVideoResponse(VideoType value) {
        return new JAXBElement<VideoType>(_AddVideoResponse_QNAME, VideoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChannelType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ttu.ee/idu0075/2015/ws/videoService", name = "getVideoChannelResponse")
    public JAXBElement<ChannelType> createGetVideoChannelResponse(ChannelType value) {
        return new JAXBElement<ChannelType>(_GetVideoChannelResponse_QNAME, ChannelType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChannelType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ttu.ee/idu0075/2015/ws/videoService", name = "addVideoChannelResponse")
    public JAXBElement<ChannelType> createAddVideoChannelResponse(ChannelType value) {
        return new JAXBElement<ChannelType>(_AddVideoChannelResponse_QNAME, ChannelType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChannelVideoListType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ttu.ee/idu0075/2015/ws/videoService", name = "getChannelVideoListResponse")
    public JAXBElement<ChannelVideoListType> createGetChannelVideoListResponse(ChannelVideoListType value) {
        return new JAXBElement<ChannelVideoListType>(_GetChannelVideoListResponse_QNAME, ChannelVideoListType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChannelVideoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ttu.ee/idu0075/2015/ws/videoService", name = "addChannelVideoResponse")
    public JAXBElement<ChannelVideoType> createAddChannelVideoResponse(ChannelVideoType value) {
        return new JAXBElement<ChannelVideoType>(_AddChannelVideoResponse_QNAME, ChannelVideoType.class, null, value);
    }

}
